import { useState, useEffect, useCallback } from 'react';

import classes from './Comments.module.css';
import NewCommentForm from './NewCommentForm';
import { useParams } from 'react-router-dom';
import useHttp from '../hooks/use-http';
import { getAllComments } from '../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import CommentsList from './CommentsList';

const Comments = () => {
  const [isAddingComment, setIsAddingComment] = useState(false);
  const params = useParams();
  const { id } = params;
  const { sendRequest, status, data } = useHttp(getAllComments);

  const startAddCommentHandler = () => {
    setIsAddingComment(true);
  };

  useEffect(() => {
    sendRequest(id);
  }, [id]);

  const addCommentHandler = useCallback(() => {
    sendRequest(id);
  }, [sendRequest, id]);
  let comments;

  if (status === 'pending') {
    comments = <div className='centered'><LoadingSpinner/></div>
  }

  if (status === 'completed' && data && data.length > 0) {
    comments = <CommentsList comments={data}/>
  }

  if (status === 'completed' && !data && data.length === 0) {
    comments = <p>No comments yet.</p>
  }
  
  return (
    <section className={classes.comments}>
      <h2>User Comments</h2>
      {!isAddingComment && (
        <button className='btn' onClick={startAddCommentHandler}>
          Add a Comment
        </button>
      )}
      {isAddingComment && <NewCommentForm onAddedComment={addCommentHandler} quoteId={id}/>}
      {comments}
    </section>
  );
};

export default Comments;
