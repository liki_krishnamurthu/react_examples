import { useParams, Route, Link, useRouteMatch } from 'react-router-dom';
import Comments from '../comments/Comments';
import HighlightedQuote from '../quotes/HighlightedQuote';
import useHttp from '../hooks/use-http';
import { getSingleQuote } from '../lib/api';
import { useEffect } from 'react';
import LoadingSpinner from '../UI/LoadingSpinner';

const QuoteDetail = () => {
    const params = useParams();
    const { id: quoteId } = params;
    const match = useRouteMatch();
    const { sendRequest, status, data: quote, error } = useHttp(getSingleQuote);

    useEffect(() => {
        sendRequest(quoteId);
    }, sendRequest, quoteId);

    if (status === 'pending') {
        return <div className='centered'>
                <LoadingSpinner/>
                </div>
    }

    if (error) {
        return <p>{error}</p>
    }

    if (!quote){
        return <p>No Quote found!</p>;
    }

    return <div>
        <HighlightedQuote text={quote.text} author={quote.author} />
        <Route path={match.path} exact>
            <div className='centered'>
                <Link className='btn--flat' to={`${match.url}/comments`}>Load Comments</Link>
            </div>
        </Route>
        <Route path={`${match.path}/comments`}>
            <Comments/>
        </Route>
    </div>
}

export default QuoteDetail;