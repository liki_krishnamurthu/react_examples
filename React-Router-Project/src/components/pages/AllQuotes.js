import QuoteList from '../quotes/QuoteList';
import { getAllQuotes } from '../lib/api';
import useHttp from '../hooks/use-http';
import { useEffect } from 'react';
import NoQuotesFound from '../quotes/NoQuotesFound';
import LoadingSpinner from '../UI/LoadingSpinner';

const AllQuotes = () => {
    const { sendRequest, status, data: allQuotes, error } = useHttp(getAllQuotes, true);

    useEffect(() => {
        sendRequest();
    }, [sendRequest]);

    if (status === 'pending') {
        return (
            <div className='centered'>
                <LoadingSpinner/>
            </div>
        )
    }

    if (error) {
        return <p className='centered focused'>{error}</p>
    }

    if (status === 'completed' && allQuotes.length === 0 || !allQuotes) {
        return <NoQuotesFound/>
    }

    return <QuoteList quotes={allQuotes}/>
}

export default AllQuotes;