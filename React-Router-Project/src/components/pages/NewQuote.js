import QuoteForm from '../quotes/QuoteForm';
import { useHistory } from 'react-router-dom';
import { addQuote } from '../lib/api';
import useHttp from '../hooks/use-http';
import { useEffect } from 'react';

const NewQuote = () => {
    const history = useHistory();
    const { sendRequest, status } = useHttp(addQuote);

    useEffect(() => {
        if (status === 'completed') {
            history.push("/quotes");
        }
    }, [status, history]);
    
    const onAddQuote = (obj) => {
        sendRequest(obj);
    }

    return <QuoteForm isLoading={status === 'pending'} onAddQuote={onAddQuote}/>
}

export default NewQuote;