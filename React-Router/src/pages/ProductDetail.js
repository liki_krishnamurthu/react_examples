
import { useParams } from 'react-router-dom';

function ProductDetail() {
    const params = useParams();
    return (
        <section>
        <h1>
                Product Detail
        </h1>
            <h2>
                {params.productId}
            </h2>
      </section>
    );
  }
  
export default ProductDetail;
  