import React, { useState } from 'react';

import MoviesList from './components/MoviesList';
import './App.css';

function App() {

  const [movies, setMovies] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  async function fetchMovieshandler() {
    try {
      setIsLoading(true);
      const response = await fetch("https://swapi.dev/api/films");

      if (response.status !== 200) {
        throw new Error("Something went wrong!");
      }
      const data = await response.json();
      const aMovies = data.results.map(item => {
        return {
          id: item.episode_id,
          title: item.title,
          releaseDate:item.release_date,
          openingText:item.opening_crawl
        }
      })
      setMovies(aMovies);
    } catch (error) {
      setError(error.message);
    }
    setIsLoading(false);
  }

  let content = <p>Found no movies.</p>

  if (movies.length > 0) {
    content = <MoviesList movies={movies} />
  }

  if (error) {
    content = <p>{error}</p>
  }

  if (isLoading) {
    content = <p>Loading...</p>
  }
  
  return (
    <React.Fragment>
      <section>
        <button onClick={fetchMovieshandler}>Fetch Movies</button>
      </section>
      <section>
        {content}
      </section>
    </React.Fragment>
  );
}

export default App;
